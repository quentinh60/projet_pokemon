<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs'],
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    Public function combat($dresseur1_id, $dresseur2_id) {
        $poke1_hp = 100;
        $poke2_hp = 100;
        $strength = 30;
        While (($poke1_hp > 0) && ($poke2_hp > 0)) {
            $poke1_hp = $poke1_hp - $this -> attaque($strength);
            if ($poke1_hp > 0) {
                $poke2_hp = $poke2_hp - $this ->attaque($strength);
            }
        }
        if ($poke1_hp > 0) {
            $winner_id = $dresseur1_id;
        }
        else {
            $winner_id = $dresseur2_id;
        }
        return $winner_id -> id;
    }

    Public function attaque($strength) {
        $precision = 90;
        $critical = 5;
        $damages = rand($strength - 10, $strength + 10);
        $attaque = rand(0, 100);
        if ($attaque > $precision) {
            $damages = 0;
        }
        else {
            if ($attaque < $critical) {
                $damages = $damages + $strength;
            }
        }
        return $damages;
    }


    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $form_data = $this->request->getData();
            $form_data['winner_dresseur_id'] = $this-> _retrieveFightWinner($form_data);
            if ($form_data['winner_dresseur_id']) {
                $fight = $this->Fights->patchEntity($fight, $form_data);
                if ($this->Fights->save($fight)) {
                    $this->Flash->success(__('The fight has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The fight could not be saved. Please, try again.'));
            }
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }

    protected function _retrieveFightWinner($form_data) {
        $dresseur1 = $this->Fights->FirstDresseurs->get($form_data['first_dresseur_id'], ['contain' => ['DresseurPokes.Pokes']]);
        $dresseur2 = $this->Fights->SecondDresseurs->get($form_data['second_dresseur_id'], ['contain' => ['DresseurPokes.Pokes']]);
        return $this -> combat($dresseur1, $dresseur2);
    }


    /**
     * Edit method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    

    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
