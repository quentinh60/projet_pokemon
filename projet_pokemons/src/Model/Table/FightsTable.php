<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fights Model
 *
 * @property \App\Model\Table\FirstDresseursTable&\Cake\ORM\Association\BelongsTo $FirstDresseurs
 * @property \App\Model\Table\SecondDresseursTable&\Cake\ORM\Association\BelongsTo $SecondDresseurs
 * @property \App\Model\Table\WinnerDresseursTable&\Cake\ORM\Association\BelongsTo $WinnerDresseurs
 *
 * @method \App\Model\Entity\Fight get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fight newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Fight[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fight|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fight saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fight patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fight[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fight findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FightsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fights');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FirstDresseurs', [
            'className' => 'Dresseurs',
            'foreignKey' => 'first_dresseur_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SecondDresseurs', [
            'className' => 'Dresseurs',
            'foreignKey' => 'second_dresseur_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('WinnerDresseurs', [
            'className' => 'Dresseurs',
            'foreignKey' => 'winner_dresseur_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['first_dresseur_id'], 'FirstDresseurs'));
        $rules->add($rules->existsIn(['second_dresseur_id'], 'SecondDresseurs'));
        $rules->add($rules->existsIn(['winner_dresseur_id'], 'WinnerDresseurs'));

        return $rules;
    }
}
