<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Log\Log;

/** 
 * LoadPokemons shell command.
 */
class LoadPokemonsShell extends Shell
{
    /** 
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $base = "https://pokeapi.co/api/v2/pokemon/";

        for ($id=1; $id <= 151; $id++) { 
            $data = file_get_contents($base.$id.'/');
            $pokemon = json_decode($data);
            $name = $pokemon->name;
            $pokedex_number = $id;

            $created = date("Y-m-d H:i:s");
            $modified = date("Y-m-d H:i:s");
            $health = 0;
            $attack = 0;
            $defense = 0;

            $tab["id"]=$id;
            $tab["name"]=$name;
            $tab["pokedex_number"]=$pokedex_number;
            $tab["created"]=$created;
            $tab["modified"]=$modified;
            $tab["health"]=$health;
            $tab["attack"]=$attack;
            $tab["defense"]=$defense;

            $this->loadModel('Pokes');
            $poke = $this->Pokes->newEntity();

            $poke = $this->Pokes->patchEntity($poke, $tab);
            $this->Pokes->save($poke);
            //Log::write('info',$poke->errors());
    }

    echo "TEST";
}
}